import { Component, OnInit } from '@angular/core';
import { RegisterUser } from 'src/app/interface/auth';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  registrationSccess:boolean = false;
  constructor(private authService:AuthService,
    private router:Router) { }

  ngOnInit(): void {
  }

  signUp(userData:RegisterUser){
    this.authService.signUp(userData).subscribe((result:any)=>{
      console.log(result);
      this.registrationSccess = true;
    })
  }

}
