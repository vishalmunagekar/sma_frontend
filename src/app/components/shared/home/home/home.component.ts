import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/interface/post';
import { PostService } from 'src/app/services/post.service';
import { UserService } from 'src/app/services/user.service';
import { UserTag } from 'src/app/interface/UserTag';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts:Post[] = []
  userTag:UserTag;
  constructor(private postService:PostService,
              private userService:UserService) { }

  ngOnInit(): void {
    this.postService.getAllFriendsPosts().subscribe((result:Post[])=>{
      this.posts = result.reverse();
    })
    this.userService.getUserTag(localStorage.username).subscribe((result:UserTag)=>{
      this.userTag = result;
    })
  }

}
