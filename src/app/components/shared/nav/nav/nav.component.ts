import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { UserTag } from 'src/app/interface/UserTag';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  isLogin:boolean = false;
  userTag:UserTag;
  constructor(private authService:AuthService, private router:Router,private userService:UserService) { }

  ngOnInit(): void {
    this.authService.subscriptionForLogin.subscribe((result:boolean)=>{
      this.isLogin = result;
    })
    this.userService.getUserTag(localStorage.username).subscribe((result:UserTag)=>{
      this.userTag = result;
    })
  }

  logout(){
    localStorage.clear();
    this.authService.subscriptionForLogin.next(false);
    this.router.navigate(['/login']);
  }

}
