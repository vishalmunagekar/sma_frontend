import { Component, OnInit, Input } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { CommentRequest } from 'src/app/interface/post';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-new-comment',
  templateUrl: './add-new-comment.component.html',
  styleUrls: ['./add-new-comment.component.css'],
})
export class AddNewCommentComponent implements OnInit {
  @Input() postCommentId: string;
  constructor(
    private postService: PostService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {}

  addNewPost(comment: HTMLInputElement) {
    let newPostComment = new CommentRequest();
    newPostComment.comment = comment.value;
    newPostComment.postCommentId = this.postCommentId;
    newPostComment.username = localStorage.username;

    this.postService
      .addNewComment(newPostComment)
      .subscribe((result: CommentRequest) => {});
    comment.value = '';
  }
}

// this.router.navigateByUrl('/comments', {skipLocationChange:true}).then(()=>{
//   this.router.navigate([decodeURI(this.location.path())]);
// })
