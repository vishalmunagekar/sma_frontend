import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirendProfileComponent } from './firend-profile.component';

describe('FirendProfileComponent', () => {
  let component: FirendProfileComponent;
  let fixture: ComponentFixture<FirendProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirendProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirendProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
