import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-firend-profile',
  templateUrl: './firend-profile.component.html',
  styleUrls: ['./firend-profile.component.css']
})
export class FirendProfileComponent implements OnInit {
  username: string = this.route.snapshot.paramMap.get('username');
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.username);

  }

}
