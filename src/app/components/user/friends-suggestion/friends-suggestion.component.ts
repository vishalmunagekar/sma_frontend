import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserTag } from 'src/app/interface/UserTag';

@Component({
  selector: 'app-friends-suggestion',
  templateUrl: './friends-suggestion.component.html',
  styleUrls: ['./friends-suggestion.component.css']
})
export class FriendsSuggestionComponent implements OnInit {
  saggestedFriends:UserTag[] = [];
  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getFriendsSaggestions().subscribe((result:UserTag[])=>{
        this.saggestedFriends = result;
    })
  }

}
