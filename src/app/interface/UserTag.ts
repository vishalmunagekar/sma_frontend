  export interface User {
        username: string;
    }

    export interface UserTag {
        userProfileId: number;
        profilePicture: string;
        occupation: string;
        user: User[];
    }
