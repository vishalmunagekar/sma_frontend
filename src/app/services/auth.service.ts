import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RefreshTokenRequest, OtpVerificationRequest, ChangePasswordRequest, RegisterUser } from '../interface/auth';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public subscriptionForLogin = new BehaviorSubject(false);
  baseUrl:string = environment.HOST_URL;
  constructor(private httpClient:HttpClient) { }

  login(credentials:any){
    return this.httpClient.post(this.baseUrl + "api/auth/login",credentials);
  }

  signUp(userData:RegisterUser){
    return this.httpClient.post(this.baseUrl + "api/auth/signup", userData);
  }

  refreshToken(){
    let refreshTokenRequest:RefreshTokenRequest = new RefreshTokenRequest();
    refreshTokenRequest.refreshToken= localStorage.refreshToken;
    refreshTokenRequest.username = localStorage.username;
    return this.httpClient.post(this.baseUrl+"api/auth/refresh/token", refreshTokenRequest);
  }

  forgotPassword(username:string){
    return this.httpClient.get(this.baseUrl + "api/auth/forgot/password/" + username);
  }

  forgotPasswordVerification(otpVerificationRequest:OtpVerificationRequest){
    return this.httpClient.post(this.baseUrl + "api/auth/forgot/password/verification", otpVerificationRequest);
  }

  changePassword(changePasswordRequest:ChangePasswordRequest){
    return this.httpClient.post(this.baseUrl + "api/auth/change/password", changePasswordRequest)
  }

}
