import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  hostUrl:string = environment.HOST_URL;
  constructor( public httpClient:HttpClient) {}

  getUserTag(username:string){
    return this.httpClient.get(this.hostUrl + "api/user/profile/tag/"+ username);
  }

  getFriendsSaggestions(){
    return this.httpClient.get(this.hostUrl + "api/user/friends/saggestions");
  }
}
